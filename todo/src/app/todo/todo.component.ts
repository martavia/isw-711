import { Component, OnInit } from '@angular/core';
import { Task } from '../models/task';
import { TodoService } from '../todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  tasks: any;
  current_task: Task;
  crud_operation = { is_new: false, is_visible: false };

  constructor(private service: TodoService) {
    // code...
  }

  ngOnInit() {
    this.service.index().subscribe(res => {
      this.tasks = res;
    });
  }

  new(){
    this.current_task = new Task();
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = true;
  }

  edit(task: Task){
    this.crud_operation.is_visible = true;
    this.crud_operation.is_new = false;
    this.current_task = task;
  }

  delete(task: Task){
  	this.service.delete(task.id).subscribe(res => {
      this.ngOnInit();
    });
  }

  save(){
    if(this.crud_operation.is_new){
      this.service.create(this.current_task).subscribe(res => {
        this.ngOnInit();
      });
    } else {
      this.service.update(this.current_task).subscribe(res => {
        this.ngOnInit();
      })
    }
    this.current_task = new Task();
    this.crud_operation.is_visible = false;
  }
}
